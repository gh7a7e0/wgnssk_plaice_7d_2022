File created:20/04/2022 17:44:27
WG assessment year:2022
Data year:2021
Stock:ple.27.7d,Stock description:Plaice (Pleuronectes platessa) in Division 7.d (eastern English Channel)
Allocation scheme:Allocation_scheme_n1

LIST OF UNSAMPLED STRATA AND THEIR ALLOCATED SAMPLED STRATA.

YOU CAN USE THIS FILE TO HAVE A MATRIX OVERVIEW OF WHAT SAMPLED STRATA HAVE BEEN ALLOCATED TO WHICH UNSAMPLED STRATA

There are 4 files:
        - 2 specifying what is allocated to the unsampled LANDINGS
            - 1 sorted by Fleet before season 
            - 1 sorted by season before Fleet 
        - 2 specifying what is allocated to the unsampled DISCARDS
            - 1 sorted by Fleet before season
            - 1 sorted by season  before Fleet 

The patterns which visually can be seen in the allocations depend the sorting of the element (season or fleet) in the strata, depending on if the allocations have been primarily done according to season or fleet.


   Explanation on how to use the output, for visual inspection:
     1. Copy all the data into a spreadsheet.
     2. Create a pivot table with the Unsampled as rows and the Sampled as columns.
     3. Copy the pivot table but only with the header row and markers, out the sum row and column and column label.
     4. Paste only values.\n
     5. Select the Sampled row and format cell and select tab Alignment and align the text horizontal and adjust the column width.
          

Unsampled BMS landing strata,Allocated sampled strata,Marker
UKE_27.7.d_GNS_DEF_all_0_0_all_1_B,FR_27.7.d_GTR_DEF_100-119_0_0_all_1_D,X
UKE_27.7.d_GNS_DEF_all_0_0_all_1_B,UKE_27.7.d_GTR_DEF_all_0_0_all_1_D,X
UKE_27.7.d_GNS_DEF_all_0_0_all_3_B,FR_27.7.d_GTR_DEF_100-119_0_0_all_3_D,X
UKE_27.7.d_GNS_DEF_all_0_0_all_3_B,FR_27.7.d_GTR_DEF_90-99_0_0_all_3_D,X
UKE_27.7.d_GNS_DEF_all_0_0_all_3_B,UKE_27.7.d_GNS_DEF_all_0_0_all_3_D,X
UKE_27.7.d_GNS_DEF_all_0_0_all_3_B,UKE_27.7.d_GTR_DEF_all_0_0_all_3_D,X
UKE_27.7.d_GTR_DEF_all_0_0_all_3_B,FR_27.7.d_GTR_DEF_100-119_0_0_all_3_D,X
UKE_27.7.d_GTR_DEF_all_0_0_all_3_B,FR_27.7.d_GTR_DEF_90-99_0_0_all_3_D,X
UKE_27.7.d_GTR_DEF_all_0_0_all_3_B,UKE_27.7.d_GNS_DEF_all_0_0_all_3_D,X
UKE_27.7.d_GTR_DEF_all_0_0_all_3_B,UKE_27.7.d_GTR_DEF_all_0_0_all_3_D,X
UKE_27.7.d_OTB_DEF_70-99_0_0_all_1_B,UKE_27.7.d_OTB_DEF_70-99_0_0_all_1_D,X
UKE_27.7.d_TBB_DEF_70-99_0_0_all_3_B,FR_27.7.d_OTB_DEF_70-99_0_0_3_D,X
UKE_27.7.d_TBB_DEF_70-99_0_0_all_3_B,UKE_27.7.d_OTB_DEF_70-99_0_0_all_3_D,X
