
1. CATON imported (if another system is used in parallel with InterCatch compare the CATON value under 1 with the CATON imported to the other system as the first check)
3893238.23 kg

2. CATON extracted (2 should be the same as 1 otherwise contact ICES Secretariat)
3893238.23 kg

3. CATON extracted with Raised discards (3 should be larger than 2, if discard is used otherwise 3 is the same as 2) kg
5441794.32

4. CATON exported (if 4 is not the same as 3, then allocations are missing for unsampled catches)
5441794.32 kg


Number of strata for which Raised discards have a CATON of 0 kg
104
