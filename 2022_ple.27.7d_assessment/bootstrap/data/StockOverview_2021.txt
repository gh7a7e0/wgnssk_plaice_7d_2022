Country	Season	Season type	Year	Stock	Area	Fleets	Effort	UnitEffort	Catch. kg	Catch Cat.	Report cat.	Discards Imported Or Raised
Germany	4	Quarter	2021	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	19663	kWd	2071.00	Landings	R-Reported	Imported
Germany	4	Quarter	2021	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	19663	kWd	5230.97	Discards	R-Reported	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	1761	kWd	35.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	1761	kWd	88.40	Discards	A-All - reported, nonre	Raised Discard
Belgium	4	Quarter	2021	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	2337	kWd	322.00	Landings	R-Reported	Imported
Belgium	4	Quarter	2021	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	2337	kWd	813.31	Discards	R-Reported	Raised Discard
UK(Scotland)	2021	Year	2021	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	267825	kWd	12951.00	Landings	R-Reported	Imported
UK(Scotland)	2021	Year	2021	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	267825	kWd	32711.85	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	5628	kWd	4754.94	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	5628	kWd	5297.55	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	13024	kWd	2050.00	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	13024	kWd	2283.93	Discards	R-Reported	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	7013.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	1335.00	Discards	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	11396.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	260.00	Discards	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	3376.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	2689.00	Discards	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	106.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	7335.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	38678.00	Discards	A-All - reported, nonre	Imported
Belgium	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	2431	kWd	67.00	Landings	R-Reported	Imported
Belgium	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	2431	kWd	74.65	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	20253	kWd	2296.60	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	20253	kWd	2558.67	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	23925	kWd	1009.68	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	23925	kWd	1124.90	Discards	R-Reported	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	165516	kWd	12.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	165516	kWd	24.19	Discards	A-All - reported, nonre	Raised Discard
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	428710	kWd	71.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	428710	kWd	143.10	Discards	A-All - reported, nonre	Raised Discard
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	347713	kWd	28.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	347713	kWd	56.43	Discards	A-All - reported, nonre	Raised Discard
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	283663	kWd	276.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	283663	kWd	556.26	Discards	A-All - reported, nonre	Raised Discard
Netherlands	2	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_100-119_0_0_all	956	kWd	10.00	Landings	R-Reported	Imported
Netherlands	2	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_100-119_0_0_all	956	kWd	20.15	Discards	R-Reported	Raised Discard
Netherlands	3	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_100-119_0_0_all	3771	kWd	90.00	Landings	R-Reported	Imported
Netherlands	3	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_100-119_0_0_all	3771	kWd	181.39	Discards	R-Reported	Raised Discard
Netherlands	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_100-119_0_0_all	2577	kWd	10.00	Landings	R-Reported	Imported
Netherlands	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_100-119_0_0_all	2577	kWd	20.15	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	16247	kWd	734.49	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	16247	kWd	1855.19	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	3041	kWd	266.20	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	3041	kWd	672.37	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	5011	kWd	278.89	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	5011	kWd	704.42	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	7407	kWd	1448.37	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	7407	kWd	3658.32	Discards	R-Reported	Raised Discard
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	759	kWd	1124.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	759	kWd	2839.02	Discards	A-All - reported, nonre	Raised Discard
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	358	kWd	233.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	358	kWd	588.52	Discards	A-All - reported, nonre	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	-9	NA	683.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	-9	NA	1725.13	Discards	A-All - reported, nonre	Raised Discard
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	402	kWd	231.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	402	kWd	583.46	Discards	A-All - reported, nonre	Raised Discard
Netherlands	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	12940	kWd	130.00	Landings	R-Reported	Imported
Netherlands	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	12940	kWd	328.36	Discards	R-Reported	Raised Discard
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	37321	kWd	13431.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	37321	kWd	6515.00	Discards	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	37321	kWd	42.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	29019.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	19809.00	Discards	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	77659	kWd	27748.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	77659	kWd	70086.35	Discards	A-All - reported, nonre	Raised Discard
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	14660.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	13563.00	Discards	A-All - reported, nonre	Imported
Netherlands	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	1905	kWd	180.00	Landings	R-Reported	Imported
Netherlands	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	1905	kWd	454.65	Discards	R-Reported	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	SDN_all_0_0_all	15300	kWd	20.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	SDN_all_0_0_all	15300	kWd	40.31	Discards	A-All - reported, nonre	Raised Discard
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	SDN_all_0_0_all	43350	kWd	419.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	SDN_all_0_0_all	43350	kWd	844.47	Discards	A-All - reported, nonre	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	OTB_SPF_70-99_0_0_all	204853	kWd	6475.09	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	OTB_SPF_70-99_0_0_all	204853	kWd	16354.89	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	OTB_SPF_70-99_0_0_all	50474	kWd	2580.62	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	OTB_SPF_70-99_0_0_all	50474	kWd	6518.17	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	OTB_SPF_70-99_0_0_all	71272	kWd	2247.21	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	OTB_SPF_70-99_0_0_all	71272	kWd	5676.04	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	OTB_SPF_70-99_0_0_all	61908	kWd	2233.91	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	OTB_SPF_70-99_0_0_all	61908	kWd	5642.45	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	971747	kWd	132929.17	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	971747	kWd	335754.65	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	971747	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	723885	kWd	160770.77	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	723885	kWd	406077.42	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	723885	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	602474	kWd	127693.34	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	602474	kWd	180889.93	Discards	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	602474	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	1269881	kWd	106504.07	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	1269881	kWd	515012.01	Discards	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	63171	kWd	14119.86	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	63171	kWd	27271.99	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	63171	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	18731	kWd	2083.49	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	18731	kWd	4024.18	Discards	R-Reported	Raised Discard
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	9238	kWd	12555.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	9238	kWd	24249.52	Discards	A-All - reported, nonre	Raised Discard
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	45432	kWd	32707.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	45432	kWd	63172.36	Discards	A-All - reported, nonre	Raised Discard
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	45432	kWd	92.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	31530	kWd	5225.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	31530	kWd	10091.89	Discards	A-All - reported, nonre	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	4276	kWd	1329.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	4276	kWd	2566.91	Discards	A-All - reported, nonre	Raised Discard
Belgium	2021	Year	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	1558656	kWd	658696.00	Landings	R-Reported	Imported
Belgium	2021	Year	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	1558656	kWd	1272247.00	Discards	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	34120	kWd	5174.72	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	34120	kWd	9994.78	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	34120	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	66502	kWd	13662.25	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	66502	kWd	26388.13	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	78102	kWd	4314.69	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	78102	kWd	2031.88	Discards	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	78102	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	71570	kWd	8604.63	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	71570	kWd	3587.74	Discards	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	71570	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	108603	kWd	13156.41	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	108603	kWd	5038.41	Discards	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	108603	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	93104	kWd	15092.91	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	93104	kWd	11918.11	Discards	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	93104	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	20900.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	23284.98	Discards	A-All - reported, nonre	Raised Discard
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	1.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	0	kWd	18966.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	0	kWd	21130.29	Discards	A-All - reported, nonre	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	23246.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	25898.70	Discards	A-All - reported, nonre	Raised Discard
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	15270.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	19395.00	Discards	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	70.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	0	kWd	79.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	0	kWd	159.22	Discards	A-All - reported, nonre	Raised Discard
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	2640	kWd	162.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	2640	kWd	326.50	Discards	A-All - reported, nonre	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	27453	kWd	99.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	27453	kWd	199.53	Discards	A-All - reported, nonre	Raised Discard
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	43474	kWd	221.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	43474	kWd	445.42	Discards	A-All - reported, nonre	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	64065	kWd	42.69	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	64065	kWd	86.04	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	676030	kWd	4642.99	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	676030	kWd	9357.73	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	2031209	kWd	24959.03	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	2031209	kWd	50303.74	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	2451480	kWd	10764.46	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	2451480	kWd	21695.26	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	138523	kWd	8506.96	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	138523	kWd	17145.37	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	57485	kWd	929.24	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	57485	kWd	1872.84	Discards	R-Reported	Raised Discard
Belgium	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	39600	kWd	523.00	Landings	R-Reported	Imported
Belgium	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	39600	kWd	1054.08	Discards	R-Reported	Raised Discard
Belgium	4	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	50400	kWd	2025.00	Landings	R-Reported	Imported
Belgium	4	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	50400	kWd	4081.29	Discards	R-Reported	Raised Discard
Netherlands	4	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	483404	kWd	31720.00	Landings	R-Reported	Imported
Netherlands	4	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	483404	kWd	63930.16	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	209568	kWd	8808.56	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	209568	kWd	17753.23	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	115018	kWd	2916.11	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	115018	kWd	5877.28	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	115018	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
Netherlands	3	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	128769	kWd	5000.00	Landings	R-Reported	Imported
Netherlands	3	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	128769	kWd	10077.26	Discards	R-Reported	Raised Discard
Netherlands	2	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	120329	kWd	1450.00	Landings	R-Reported	Imported
Netherlands	2	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	120329	kWd	2922.41	Discards	R-Reported	Raised Discard
Netherlands	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	345230	kWd	12070.00	Landings	R-Reported	Imported
Netherlands	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	345230	kWd	24326.51	Discards	R-Reported	Raised Discard
Belgium	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0_all	2722	kWd	98.00	Landings	R-Reported	Imported
Belgium	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0_all	2722	kWd	247.53	Discards	R-Reported	Raised Discard
Belgium	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0_all	4891	kWd	176.00	Landings	R-Reported	Imported
Belgium	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0_all	4891	kWd	444.54	Discards	R-Reported	Raised Discard
Belgium	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_32-69_0_0_all	25200	kWd	377.00	Landings	R-Reported	Imported
Belgium	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_32-69_0_0_all	25200	kWd	759.83	Discards	R-Reported	Raised Discard
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	89674	kWd	188.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	89674	kWd	378.91	Discards	A-All - reported, nonre	Raised Discard
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	563707	kWd	1458.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	563707	kWd	2938.53	Discards	A-All - reported, nonre	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	701939	kWd	3969.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	701939	kWd	7999.33	Discards	A-All - reported, nonre	Raised Discard
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	572431	kWd	2091.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	572431	kWd	4214.31	Discards	A-All - reported, nonre	Raised Discard
UK(Scotland)	2021	Year	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	29482	kWd	2378.00	Landings	R-Reported	Imported
UK(Scotland)	2021	Year	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	29482	kWd	4792.75	Discards	R-Reported	Raised Discard
Belgium	1	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	5037	kWd	394.00	Landings	R-Reported	Imported
Belgium	1	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	5037	kWd	794.09	Discards	R-Reported	Raised Discard
Belgium	4	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	5694	kWd	96.00	Landings	R-Reported	Imported
Belgium	4	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	5694	kWd	193.48	Discards	R-Reported	Raised Discard
Netherlands	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF	1256	kWd	60.00	Landings	R-Reported	Imported
Netherlands	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF	1256	kWd	151.55	Discards	R-Reported	Raised Discard
Netherlands	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF	2418	kWd	130.00	Landings	R-Reported	Imported
Netherlands	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF	2418	kWd	328.36	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	DRB_all_0_0_all	948	kWd	435.00	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	DRB_all_0_0_all	948	kWd	876.72	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	DRB_all_0_0_all	1372	kWd	387.10	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	DRB_all_0_0_all	1372	kWd	780.18	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	DRB_all_0_0_all	2302	kWd	739.22	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	DRB_all_0_0_all	2302	kWd	1489.86	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	DRB_all_0_0_all	352	kWd	13.00	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	DRB_all_0_0_all	352	kWd	26.20	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	14351	kWd	805.63	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	14351	kWd	897.56	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	20323	kWd	3555.94	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	20323	kWd	3961.72	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	20323	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	6365	kWd	3346.25	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	6365	kWd	3728.10	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	23018	kWd	1428.00	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	23018	kWd	1590.95	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	23018	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	168050	kWd	951.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	168050	kWd	1916.70	Discards	A-All - reported, nonre	Raised Discard
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	239714	kWd	5614.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	239714	kWd	11314.75	Discards	A-All - reported, nonre	Raised Discard
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	226705	kWd	6686.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	226705	kWd	13475.32	Discards	A-All - reported, nonre	Raised Discard
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	16545	kWd	187.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2021	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	16545	kWd	376.89	Discards	A-All - reported, nonre	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	5293	kWd	1363.67	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	5293	kWd	3444.38	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	15078	kWd	852.96	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	15078	kWd	2154.42	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	43832	kWd	4215.87	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	43832	kWd	10648.51	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	6976	kWd	1325.02	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	6976	kWd	3346.76	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	17566	kWd	1670.02	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	17566	kWd	4218.16	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	13393	kWd	637.00	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	13393	kWd	1608.94	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	9144	kWd	845.01	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	9144	kWd	2134.34	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	2615	kWd	478.75	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	2615	kWd	1209.23	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	96998	kWd	5591.48	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	96998	kWd	4567.85	Discards	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	96998	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	43204	kWd	3415.03	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	43204	kWd	5591.66	Discards	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	43204	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	105554	kWd	5218.68	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	105554	kWd	11959.04	Discards	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	105554	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	70452	kWd	3783.06	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	70452	kWd	8333.44	Discards	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	70452	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	2239	kWd	5.00	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	2239	kWd	5.57	Discards	R-Reported	Raised Discard
France	1	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	1874	kWd	5.20	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	1874	kWd	5.79	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	9984	kWd	321.49	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	9984	kWd	358.18	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	3588	kWd	69.16	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	3588	kWd	77.05	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	2075812	kWd	7502.26	Landings	R-Reported	Imported
France	2	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	2075812	kWd	15120.45	Discards	R-Reported	Raised Discard
France	2	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	2075812	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	3038184	kWd	7663.00	Landings	R-Reported	Imported
France	1	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	3038184	kWd	15444.41	Discards	R-Reported	Raised Discard
France	4	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	3648532	kWd	10198.44	Landings	R-Reported	Imported
France	4	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	3648532	kWd	20554.47	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	1550466	kWd	11545.57	Landings	R-Reported	Imported
France	3	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	1550466	kWd	23269.55	Discards	R-Reported	Raised Discard
France	3	Quarter	2021	ple.27.7d	27.7.d	MIS_MIS_0_0_0	1550466	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
